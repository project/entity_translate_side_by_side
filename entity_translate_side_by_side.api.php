<?php

/**
 * @file
 * Hooks provided by the Entity Translate Side by Side module.
 */

/**
 * Alters the list of entity types that should be skipped for side-by-side translation.
 *
 * @param array $skip
 *   An array of entity type IDs to skip. Modify this array to change which
 *   entity types will be skipped.
 */
function hook_entity_translate_side_by_side_skip_alter(array &$skip): void {
  // A module could add this entry to skip its own entity type.
  $skip[] = 'my_custom_entity_type';
}
