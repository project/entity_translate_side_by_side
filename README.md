ENTITY TRANSLATE SIDE BY SIDE
--------------

Edit multiple translations of the same entity side-by-side.

Please install the module using the composer workflow:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules


DRUPAL VERSION
--------------

Drupal 10.1 and up


PHP VERSION
-----------------------------

PHP 8.1 and up, just like Drupal 10.1.


CONFIGURING DEFAULT LANGUAGES
--------------------------

1. Go to: /admin/config/system/entity-translate-side-by-side
2. Select the languages to be loaded by default


CREDITS
-------

Entity Translate Side by Side has been written by:

- Daniel Ehrenhofer [(danielehrenhofer)](https://www.drupal.org/u/danielehrenhofer)
- Markus Kalkbrenner [(mkalkbrenner)](https://www.drupal.org/u/mkalkbrenner)


SPONSORS
--------

Entity Translate Side by Side:
- [Jarltech Europe GmbH](https://www.drupal.org/jarltech-europe-gmbh)


CONTACT
-------
Developed and maintained by [Jarltech Europe GmbH](http://jarltech.com).
