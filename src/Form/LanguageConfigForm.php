<?php

namespace Drupal\entity_translate_side_by_side\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for configuring languages available for translation.
 */
class LanguageConfigForm extends ConfigFormBase {

  /**
   * The language manager.
   */
  protected LanguageManager $languageManager;

  /**
   * Builds the configuration form.
   *
   * @param array<string, mixed> $form
   *   The initial form array.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array<string, mixed>
   *   The built form array.
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('entity_translate_side_by_side.settings');

    // Gets all available languages.
    $languages = $this->languageManager->getLanguages();
    $language_options = [];

    foreach ($languages as $langcode => $language) {
      $language_options[$langcode] = $language->getName();
    }

    // Checks whether languages are already saved in the configuration.
    $saved_languages = $config->get('languages');

    if (empty($saved_languages)) {
      // If no languages are saved, use the system's default language.
      $default_language = $this->languageManager->getDefaultLanguage();
      $default_value = [$default_language->getId()];
    }
    else {
      // Uses the saved languages as the default value.
      $default_value = $saved_languages;
    }

    $form['description'] = [
      '#markup' => $this->t('This configuration form allows administrators to select the languages that will be available for translation in the Entity Translate Side by Side module. The listed languages are those installed and enabled on your Drupal site. Select the languages you wish to be available for translation, which will be reflected in the translation interface of the module.'),
    ];

    $form['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages to edit'),
      '#default_value' => $default_value,
      '#options' => $language_options,
      '#description' => $this->t('Select the languages that should be editable by default.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->languageManager = $container->get('language_manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'entity_translate_side_by_side_admin_settings';
  }

  /**
   * Submits the configuration form.
   *
   * @param array<string, mixed> &$form
   *   The form array.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('entity_translate_side_by_side.settings')
      ->set('languages', array_filter($form_state->getValue('languages')))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   *
   * @return array<int, string>
   *   An array of configuration object names that are editable if called in
   *    conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames(): array {
    return [
      'entity_translate_side_by_side.settings',
    ];
  }

}
