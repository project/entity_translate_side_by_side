<?php

namespace Drupal\entity_translate_side_by_side\Form;

use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for side-by-side translation of entities.
 */
class EntityTranslateSideBySideForm extends FormBase {

  /**
   * Fields that should not be displayed or processed.
   *
   * TODO: Consider making this blacklist configurable for the contribution module.
   *  This would allow administrators to customize which fields should be excluded
   *  from processing in the Entity Translate Side by Side module.
   *  Issue: https://www.drupal.org/project/entity_translate_side_by_side/issues/3416987
   *
   * @var string[]
   */
  protected array $blacklist = [
    'langcode',
    'uid',
    'status',
    'created',
    'changed',
    'default_langcode',
    'content_translation_source',
    'content_translation_outdated',
    'layout_builder__layout',
  ];

  /**
   * The system default language.
   */
  protected string $defaultLanguage;

  /**
   * Constructs a new EntityTranslateSideBySideForm object.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param LanguageManagerInterface $languageManager
   *   The language manager service.
   */
  public function __construct(
    /**
     * The entity type manager.
     */
    protected EntityTypeManagerInterface $entityTypeManager,
    /**
     * The language manager.
     */
    protected LanguageManagerInterface $languageManager
  ) {
    $this->defaultLanguage = $this->languageManager->getDefaultLanguage()->getId();
  }

  /**
   * Builds the translation form with side-by-side layout for different languages.
   *
   * @param array<string, mixed> $form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   * @param string|null $entity_type
   *   The entity type being translated.
   * @param int|null $entity_id
   *   The ID of the entity being translated.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @return array<string, mixed>
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?string $entity_type = NULL, ?int $entity_id = NULL): array {
    // Check whether the entity type exists
    if (!$this->entityTypeManager->hasDefinition($entity_type)) {
      $this->messenger()->addError($this->t('The entity type @entity_type does not exist.', ['@entity_type' => $entity_type]));

      return $form;
    }

    // Load the entity based on type and ID.
    $entity = $this->entityTypeManager->getStorage($entity_type)
      ->load($entity_id);

    if (!$entity) {
      $this->messenger()->addError($this->t('The requested entity could not be found.'));

      return $form;
    }

    $languages = $this->languageManager->getLanguages();
    $language_options = [];

    foreach ($languages as $langcode => $language) {
      $language_options[$langcode] = $language->getName();
    }

    // Load the configuration languages or, if not available, the entity languages.
    $config = $this->config('entity_translate_side_by_side.settings');
    $default_languages = $config->get('languages');

    // If no standard languages are defined in the configuration, use the translation languages of the entity.
    if (empty($default_languages)) {
      // @phpstan-ignore-next-line
      $entity_translation_languages = array_keys($entity->getTranslationLanguages());

      // If no translation languages are available, use the default language.
      if (empty($entity_translation_languages)) {
        $default_languages = [$this->languageManager->getDefaultLanguage()->getId()];
      }
      else {
        $default_languages = $entity_translation_languages;
      }
    }

    $form['entity_type'] = [
      '#type' => 'hidden',
      '#value' => $entity_type,
    ];

    $form['entity_id'] = [
      '#type' => 'hidden',
      '#value' => $entity_id,
    ];

    $form['form_info'] = [
      '#markup' => $this->t('Select the languages you want to work with and provide translations in the fields below.'),
      '#prefix' => '<div class="info-message">',
      '#suffix' => '</div>',
    ];

    $selected_languages = \Drupal::request()->query->get('langcodes');

    if ($selected_languages) {
      $selected_languages = explode(',', $selected_languages);
    }
    else {
      $selected_languages = $default_languages;
    }

    // @phpstan-ignore-next-line
    $fields = $entity->getFieldDefinitions();

    // Create form elements for each supported language and field
    $form = $this->addFieldsForSelectedLanguages($selected_languages, $form_state, $form, $entity, $fields);
    $form = $this->getLanguageSelectDetailsSummary($form, $language_options, $selected_languages);

    if ($form['#fields_have_been_added']) {
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save translations'),
        '#button_type' => 'primary',
      ];
    }

    $form['#attached']['library'][] = 'entity_translate_side_by_side/edit_form_styles';
    $form['#attached']['library'][] = 'entity_translate_side_by_side/edit_form_scripts';

    return $form;
  }

  /**
   * {@inheritdoc}
   *
   * @param ContainerInterface $container
   *   The service container.
   *
   * @return EntityTranslateSideBySideForm|static
   *   A new instance of the EntityTranslateSideBySideForm.
   */
  public static function create(ContainerInterface $container): EntityTranslateSideBySideForm|static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'entity_translate_side_by_side_form';
  }

  /**
   * Form submission handler.
   *
   * Processes the translation submission, saving translations for the selected languages.
   *
   * @param array<string, mixed> &$form
   *   An associative array containing the structure of the form.
   * @param FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $entity_type = $form_state->getValue('entity_type');
    $entity_id = $form_state->getValue('entity_id');

    // Resetting the entity cache is necessary here because addTranslation() in the buildForm
    // might introduce temporary translations that are not yet saved to the database.
    // This can lead to inconsistent entity states in subsequent operations. By resetting the cache,
    // we ensure that we are working with the most accurate and up-to-date version of the entity
    // as it exists in the database, free from any unsaved temporary states.
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $storage->resetCache([$entity_id]);
    $entity = $storage->load($entity_id);

    if (!$entity) {
      $this->messenger()->addError($this->t('Unable to load the entity.'));

      return;
    }

    // @phpstan-ignore-next-line
    $default_translation = $entity->getTranslation($this->defaultLanguage);
    $is_default_translation_published = $default_translation->isPublished();

    $selected_languages = array_filter($form_state->getValue('language_selection'));
    // Track if changes were made at all.
    $changes_made = FALSE;
    $updated_languages = [];
    $updated_fields_by_language = [];

    foreach ($selected_languages as $langcode) {
      // Check whether the translation already exists.
      // @phpstan-ignore-next-line
      $is_new_translation = !$entity->hasTranslation($langcode);
      // @phpstan-ignore-next-line
      $translation = $is_new_translation ? $entity->addTranslation($langcode) : $entity->getTranslation($langcode);

      $updated_fields = [];

      // @phpstan-ignore-next-line
      foreach ($translation->getFieldDefinitions() as $field_name => $field_definition) {
        $new_field_value = $form_state->getValue([$langcode, $field_name]);
        $old_field_value = $translation->get($field_name)->getValue();

        if ($this->isFieldEditable($field_definition, $field_name)) {
          // Track if changes were made at the current field level.
          $changes_at_field_level_made = FALSE;

          if ($this->shouldSaveChanges($new_field_value, $old_field_value, $field_definition)) {
            $translation->set($field_name, $new_field_value);
            $changes_made = TRUE;
            $changes_at_field_level_made = TRUE;
          }

          if ($changes_at_field_level_made) {
            $updated_fields[] = $field_name;
          }

          continue;
        }

        // If a field value is empty and the field is required, it's necessary to set a default value.
        // This avoids issues with null pointer exceptions when the entity is saved.
        // Here, we use the value from the default translation as the default value.
        if (empty($new_field_value) && $field_definition->isRequired()) {
          $new_field_value = $default_translation->get($field_name)->getValue();
          $translation->set($field_name, $new_field_value);
        }
      }

      if (!empty($updated_fields)) {
        $updated_fields_by_language[$langcode] = $updated_fields;
        $updated_languages[] = $langcode;
      }

      if ($changes_made) {
        if ($is_default_translation_published) {
          // Only publish if the standard translation has been published.
          // If it is a new translation, the translation is activated by default.
          // Otherwise, the current status is retained.
          $current_status = $is_new_translation ? 1 : $translation->isPublished();
        }
        else {
          // Do not publish if the standard translation is not published.
          $current_status = 0;
        }

        $translation->set('status', $current_status);
        $translation->save();
      }
    }

    if ($changes_made) {
      $details_list = [];

      foreach ($updated_languages as $langcode) {
        $language_name = $this->languageManager->getLanguage($langcode)->getName();

        $updated_field_labels = array_map(static fn ($field_name) =>
            // @phpstan-ignore-next-line
            $entity->getFieldDefinition($field_name)->getLabel(), $updated_fields_by_language[$langcode]);

        $fields_list = implode('</li><li>', $updated_field_labels);
        $details_list[] = '<strong class="success-message-language-name-display-block">' . $language_name . ':</strong> <ul><li>' . $fields_list . '</li></ul>';
      }

      $formatted_details = Markup::create(implode('', $details_list));
      $this->messenger()->addMessage($this->t('Updated translations and fields: @details', ['@details' => $formatted_details]));
    }
    else {
      $this->messenger()->addMessage($this->t('No changes to save.'), 'warning');
    }

    // Extract the 'destination' parameter from the current URL and set the redirect URL of the form.
    $destination = $this->getRequest()->query->get('destination');

    if ($destination) {
      $form_state->setRedirectUrl(Url::fromUri('internal:/' . $destination));
    }
  }

  /**
   * Adds fields for selected languages to the form.
   *
   * When analyzing the entity fields, it is decided which field will be offered for translation and how it must be presented.
   * Moreover, it is determined whether translations already exist, and if they do, the stored values are displayed.
   * If no translations or values exist, empty fields are displayed according to the standard language.
   *
   * @param mixed $selected_languages
   *   The languages selected by the user for translation.
   * @param FormStateInterface $form_state
   *   The form state.
   * @param array<string, mixed> $form
   *   The form to which fields will be added.
   * @param EntityInterface $entity
   *   The entity being translated.
   * @param array<string, mixed> $fields
   *   The field definitions for the entity.
   *
   * @return array<string, mixed>
   *   The modified form structure.
   */
  protected function addFieldsForSelectedLanguages(mixed $selected_languages, FormStateInterface $form_state, array $form, EntityInterface $entity, array $fields): array {
    // @phpstan-ignore-next-line
    $display = EntityFormDisplay::collectRenderDisplay($entity, 'edit');
    $form['language_fields'] = $this->initializeLanguageFieldsContainer();
    $sorted_fields = $this->createSortedListOfFields($entity, $fields);
    $form['#fields_have_been_added'] = FALSE;

    foreach ($selected_languages as $langcode) {
      // @phpstan-ignore-next-line
      $entity_has_translation = $entity->hasTranslation($langcode);

      $helper_form = [];
      // @phpstan-ignore-next-line
      $display->buildForm($entity_has_translation ? $entity->getTranslation($langcode) : $entity->addTranslation($langcode), $helper_form, $form_state);

      // Recursively update the #parents property for this element and all subordinate elements
      foreach ($helper_form as &$field_element) {
        $this->updateChildElementParents($field_element, $langcode);
      }

      $form['language_fields'][$langcode] = $this->initializeLanguageContainer($langcode, $entity_has_translation);

      // Iterate over all fields of the entity.
      foreach ($sorted_fields as $field_name => $field_definition) {
        // Check if the field is at all editable.
        if ($this->isFieldEditable($field_definition, $field_name)) {
          $form['#fields_have_been_added'] = TRUE;
          $form['language_fields'][$langcode][$field_name] = $helper_form[$field_name];
        }
      }
    }

    if (!$form['#fields_have_been_added']) {
      $this->messenger()->addMessage($this->t('No fields could be loaded for translation.'), 'warning');
    }

    $form['description'] = [
      '#markup' => $this->t('You can horizontally scroll within the box to reach the desired language. Additionally, the drag-and-drop feature allows for easy navigation: simply click and hold inside the box, and then drag to the left or right to move through the different language options.'),
      '#prefix' => '<small class="scroll-box-description">',
      '#suffix' => '</small>',
    ];

    return $form;
  }

  /**
   * Check if two arrays are effectively equal.
   *
   * Two arrays are considered effectively equal if they are
   * the same when empty strings and nulls are ignored.
   *
   * @param array<string, mixed> $array1
   *   The first array.
   * @param array<string, mixed> $array2
   *   The second array.
   *
   * @return bool
   *   True if the arrays are effectively equal, false otherwise.
   */
  protected function arraysAreEffectivelyEqual(array $array1, array $array2): bool {
    // Sort the arrays by keys
    $this->recursiveKsort($array1);
    $this->recursiveKsort($array2);

    // Both values are "empty" in terms of requirements
    if ($this->isEffectivelyEmpty($array1) && $this->isEffectivelyEmpty($array2)) {
      return TRUE;
    }

    // Check if both variables are arrays
    if (!\is_array($array1) || !\is_array($array2)) {
      return $array1 === $array2;
    }

    // Check if the arrays have the same number of elements
    if (\count($array1) !== \count($array2)) {
      return FALSE;
    }

    // Element-by-element comparison of the arrays
    foreach ($array1 as $key => $value1) {
      // Check whether both arrays have the key
      if (!\array_key_exists($key, $array2)) {
        return FALSE;
      }
      $value2 = $array2[$key];

      // Recursive comparison if the values are arrays
      if (\is_array($value1) && \is_array($value2)) {
        if (!$this->arraysAreEffectivelyEqual($value1, $value2)) {
          return FALSE;
        }
      }
      else {
        // Direct comparison of the values
        // NULL and empty strings are considered equal
        if ((empty($value1) && empty($value2)) || $value1 === $value2) {
          continue;
        }

        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Convert the Checkbox value to string.
   *
   * This function is necessary because the structure of the field value can vary
   * depending on how it's submitted or stored. For instance, checkbox values might be
   * stored as integers (1 or 0) or strings ('1' or '0'), and they might be stored
   * directly or within a nested array structure. This function standardizes the value
   * by converting it to a string and handling the possible structural variations,
   * ensuring consistent comparison in the shouldSaveChanges function.
   *
   * @param array<string, mixed> $value
   *   The value or array of values to convert.
   * @param string $key_to_check
   *   The specific key to check within the array.
   *
   * @return string
   *   The converted value as a string, or an empty string if the key isn't set.
   */
  protected function convertCheckboxValueToString(array $value, string $key_to_check): string {
    // Check whether the value is directly available
    if (isset($value[$key_to_check])) {
      return (string) $value[$key_to_check];
    }

    // Check whether the value is present in an array under index 0
    // @phpstan-ignore-next-line
    if (isset($value[0][$key_to_check]) && \is_array($value[0])) {
      return (string) $value[0][$key_to_check];
    }

    // Returns an empty string if the value was not found
    return '';
  }

  /**
   * Creates a sorted list of field definitions for an entity based on the form display configuration.
   *
   * This function retrieves the form display configuration for the given entity and sorts the field
   * definitions according to the order specified in the configuration (based on 'weight'). If the
   * entity's form display configuration is not available or doesn't define an order for some fields,
   * those fields are included in their original order as a fallback.
   *
   * @param EntityInterface $entity
   *   The entity for which the sorted list of fields is to be created.
   * @param array<string, mixed> $fields
   *   An associative array of the entity's field definitions.
   *
   * @return array<string, mixed>
   *   An associative array of the entity's field definitions sorted according to their 'weight' in
   *   the form display configuration.
   */
  protected function createSortedListOfFields(EntityInterface $entity, array $fields): array {
    if (!$entity instanceof FieldableEntityInterface) {
      return $fields;
    }

    $form_display = EntityFormDisplay::collectRenderDisplay($entity, 'default');

    if ($form_display instanceof EntityFormDisplayInterface) {
      $form_display_components = $form_display->getComponents();

      // Filter out components that are not present in the fields array
      $relevant_components = array_intersect_key($form_display_components, $fields);

      // Sort the components by weight
      uasort($relevant_components, static fn ($a, $b) => $a['weight'] <=> $b['weight']);

      // Extract the keys (field names) from the sorted components
      $sorted_field_names = array_keys($relevant_components);

      // Create a sorted list of fields based on the sorted field names
      $sorted_fields = [];

      foreach ($sorted_field_names as $field_name) {
        if (isset($fields[$field_name])) {
          $sorted_fields[$field_name] = $fields[$field_name];
        }
      }

      return $sorted_fields;
    }

    // @phpstan-ignore-next-line
    return $fields;
  }

  /**
   * Retrieves the name of a language given its language code.
   *
   * @param string $langcode
   *   The language code.
   *
   * @return string
   *   The name of the language.
   */
  protected function getLanguageName(string $langcode): string {
    $language = $this->languageManager->getLanguage($langcode);

    return $language ? $language->getName() : 'Unknown Language';
  }

  /**
   * Builds the summary section of the form for language selection.
   *
   * @param array<string, mixed> $form
   *   The form to which the summary will be added.
   * @param array<string, mixed> $language_options
   *   The available language options.
   * @param mixed $default_languages
   *   The default selected languages.
   *
   * @return array<string, mixed>
   *   The modified form structure.
   */
  protected function getLanguageSelectDetailsSummary(array $form, array $language_options, mixed $default_languages): array {
    $form['language_details'] = [
      '#type' => 'details',
      '#title' => $this->t('Select languages to be translated'),
      '#open' => FALSE,
    ];

    $form['language_details']['language_selection'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages to edit'),
      '#options' => $language_options,
      '#default_value' => $default_languages,
    ];

    // This section creates a span element styled as a button specifically for updating the language selection.
    // Unlike a traditional 'submit' button, this span is designed to trigger a JavaScript function
    // rather than initiate a full form submission. This approach is advantageous in scenarios
    // where we want to refresh or rebuild the form based on certain user selections (like language preferences)
    // without going through the entire form submission process.
    //
    // Using a regular submit button would typically involve server-side processing,
    // including form validation and submission handling, which might not be necessary
    // for simply updating part of the form's content (in this case, based on selected languages).
    $form['language_details']['update'] = [
      '#markup' => '<span id="update-languages-link" class="button button--secondary">' . $this->t('Update Languages') . '</span>',
    ];

    $form['language_details']['unsaved_changes_will_be_reset'] = [
      '#markup' => '<strong>' . $this->t('Attention!') . '</strong> ' . $this->t('Adding new languages will reset the unsaved changes.'),
      '#prefix' => '<small class="info-message-unsaved-changes-will-be-reset">',
      '#suffix' => '</small>',
    ];

    // Add a link to the admin settings if the user has the authorization.
    if (\Drupal::currentUser()
      ->hasPermission('administer site configuration')) {
      $admin_settings_url = Url::fromRoute('entity_translate_side_by_side.admin_settings');
      $form['language_details']['admin_link'] = [
        '#markup' => $this->t('Manage default language settings <a href=":url">here</a>.', [':url' => $admin_settings_url->toString()]),
        '#prefix' => '<small class="info-message-administer-site-configuration">',
        '#suffix' => '</small>',
      ];
    }

    return $form;
  }

  /**
   * Initializes a container for the fields of a language.
   *
   * @param string $langcode
   *   The current language.
   * @param bool $entity_has_translation
   *   Flag whether there is already a translation for the entity.
   *
   * @return array<string, mixed>
   */
  protected function initializeLanguageContainer(string $langcode, bool $entity_has_translation): array {
    $language_name = $this->getLanguageName($langcode);
    $language_title_markup = '<h6>' . $language_name . (!$entity_has_translation ? ' (' . $this->t('New translation') . ')' : '') . '</h6>';

    return [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'language-edit-wrapper-' . $langcode,
        'class' => ['language-edit-fields'],
      ],
      'language_title' => [
        '#markup' => $language_title_markup,
      ],
    ];
  }

  /**
   * Initializes the container for language fields.
   *
   * @return array<string, mixed>
   *   The container element.
   */
  protected function initializeLanguageFieldsContainer(): array {
    return [
      '#type' => 'container',
      '#attributes' => ['id' => 'languages-edit-wrapper'],
      '#tree' => TRUE,
    ];
  }

  /**
   * Check if a value is effectively empty.
   *
   * A value is considered effectively empty if it is null,
   * an empty string, or an array that only contains empty values.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   True if the value is effectively empty, false otherwise.
   */
  protected function isEffectivelyEmpty(mixed $value): bool {
    // An array is considered empty if all its values are empty
    if (\is_array($value)) {
      foreach ($value as $subValue) {
        if (\is_array($subValue)) {
          if (!$this->isEffectivelyEmpty($subValue)) {
            return FALSE;
          }
        }
        elseif (!$this->isEmptyValue($subValue)) {
          return FALSE;
        }
      }

      return TRUE;
    }

    // For non-arrays, check if the value is null or an empty string
    return $value === NULL || $value === '';
  }

  /**
   * Check if a value is null, an empty string, or an empty array.
   *
   * @param mixed $value
   *   The value to check.
   *
   * @return bool
   *   True if the value is null, an empty string, or an empty array, false otherwise.
   */
  protected function isEmptyValue(mixed $value): bool {
    return $value === NULL || $value === '' || (\is_array($value) && empty($value));
  }

  /**
   * Determines whether a field is editable.
   *
   * @param FieldDefinitionInterface $field_definition
   *   The field definition.
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   TRUE if the field is editable, otherwise FALSE.
   */
  protected function isFieldEditable(FieldDefinitionInterface $field_definition, string $field_name): bool {
    return $field_definition->isTranslatable()
      && !$field_definition->isReadOnly()
      && !$field_definition->isComputed()
      && !\in_array($field_name, $this->blacklist, TRUE)
      && !$field_definition->getFieldStorageDefinition()->isBaseField();
  }

  /**
   * Recursively sort an array by its keys.
   *
   * @param array<string, mixed> &$array
   *   The array to sort.
   */
  protected function recursiveKsort(array &$array): void {
    ksort($array);

    foreach ($array as &$value) {
      if (\is_array($value)) {
        $this->recursiveKsort($value);
      }
    }
  }

  /**
   * Determine if changes between the new and old field values should be saved.
   *
   * @param mixed $new_field_value
   *   The new value of the field.
   * @param mixed $old_field_value
   *   The old (current) value of the field.
   * @param FieldDefinitionInterface $field_definition
   *   The field definition.
   *
   * @return bool
   *   True if changes should be saved, false otherwise.
   */
  protected function shouldSaveChanges(mixed $new_field_value, mixed $old_field_value, FieldDefinitionInterface $field_definition): bool {
    $field_type = $field_definition->getType();
    $key_to_check = NULL;

    // Special logic for certain field types
    switch ($field_type) {
      case 'image':
      case 'file':
      case 'entity_reference':
        $key_to_check = 'target_id';

        break;

      case 'text':
      case 'string':
      case 'text_with_summary':
        $key_to_check = 'value';

        // Check whether it is a multi-value text field.
        if ($field_definition->getFieldStorageDefinition()->getCardinality() !== 1) {
          // If it is a multi-value text field, then all unused child text fields must be removed.
          foreach ($new_field_value as $delta => $value) {
            if (empty($value['value'])) {
              unset($new_field_value[$delta]);
            }
          }
        }

        break;

      case 'boolean':
        $key_to_check = 'value';
        $new_value = $this->convertCheckboxValueToString($new_field_value, $key_to_check);
        $old_value = $this->convertCheckboxValueToString($old_field_value, $key_to_check);

        return $new_value !== $old_value;
    }

    // Check if both values are effectively empty (null, empty string, empty array)
    if ($this->isEffectivelyEmpty($new_field_value) && $this->isEffectivelyEmpty($old_field_value)) {
      return FALSE;
    }

    // Check if the new value is effectively empty but the old value is not (deletion process)
    if ($this->isEffectivelyEmpty($new_field_value) && !$this->isEffectivelyEmpty($old_field_value)) {
      return TRUE;
    }

    // Check if the old value is effectively empty (e.g., null, empty string, or empty array) but the new value is not.
    // This situation typically indicates an addition process where a new value is being provided for a field that previously had no value or is to be changed.
    if (!$this->isEffectivelyEmpty($new_field_value) && $this->isEffectivelyEmpty($old_field_value)) {
      // If a key to check has been identified (i.e., it's not NULL), verify that the key exists in the new field value
      // and that the corresponding value is not empty. This ensures that only significant changes (non-empty values)
      // are considered worth saving. This check helps prevent saving empty or insignificant data that doesn't provide
      // meaningful information for the field.
      // Returns true if a significant change is detected (key exists and value is not empty), indicating that the change should be saved.
      // Returns false if the key is not set or the value is empty, indicating that the change should not be saved.
      if ($key_to_check !== NULL) {
        return isset($new_field_value[0][$key_to_check]) && $new_field_value[0][$key_to_check] !== '';
      }

      return TRUE;
    }

    // Check if the values are arrays and whether they are effectively equal
    if (\is_array($new_field_value) && \is_array($old_field_value) && $this->arraysAreEffectivelyEqual($new_field_value, $old_field_value)) {
      return FALSE;
    }

    // Default case: Save if the values are different
    return $new_field_value !== $old_field_value;
  }

  /**
   * Recursively updates the #parents property of a form element and its children.
   *
   * @param mixed $element
   *   The form element array, passed by reference.
   * @param string $langcode
   *   The language code to be prefixed to the #parents array.
   */
  protected function updateChildElementParents(mixed &$element, string $langcode): void {
    // Check whether the element is an array and the #parents property is present
    if (\is_array($element) && isset($element['#parents'])) {
      // Insert the language code as the first element in the #parents array
      array_unshift($element['#parents'], $langcode);

      // Go through all subordinate elements
      foreach ($element as &$child) {
        // Recursion for subordinate elements
        if (\is_array($child)) {
          $this->updateChildElementParents($child, $langcode);
        }
      }

      // Remove reference to child element
      unset($child);
    }
  }

}
