<?php

namespace Drupal\entity_translate_side_by_side\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines a breadcrumb builder for the Entity Translate Side by Side module.
 */
class EntityTranslateSideBySideBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager service.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The request stack to get the current request for the destination parameter.
   */
  protected RequestStack $requestStack;

  /**
   * Constructs a new EntityTranslateSideBySideBreadcrumbBuilder object.
   *
   * @param EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param RequestStack $requestStack
   *   The request stack to get the current request for destination parameter.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, RequestStack $requestStack) {
    $this->entityTypeManager = $entityTypeManager;
    $this->requestStack = $requestStack;
  }

  /**
   * Determines if the breadcrumb builder should be used for the current route.
   *
   * @param RouteMatchInterface $route_match
   *   The current route match.
   *
   * @return bool
   *   TRUE if the breadcrumb builder applies to the current route, FALSE otherwise.
   */
  public function applies(RouteMatchInterface $route_match): bool {
    return $route_match->getRouteName() === 'entity_translate_side_by_side';
  }

  /**
   * Builds the breadcrumb.
   *
   * @param RouteMatchInterface $route_match
   *   The route match.
   *
   * @return Breadcrumb
   *   A breadcrumb trail for the given route.
   */
  public function build(RouteMatchInterface $route_match): Breadcrumb {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addLink(Link::createFromRoute($this->t('Home'), '<front>'));

    // Attempt to use the 'destination' parameter from the query to add a "Previous page" breadcrumb.
    $destination = $this->requestStack->getCurrentRequest()->query->get('destination');

    if ($destination) {
      // Ensure the destination is a routed URL within the site.
      $url = Url::fromUserInput('/' . $destination);

      if ($url->isRouted()) {
        $breadcrumb->addLink(new Link($this->t('Previous page'), $url));
      }
    }

    // Add the current page title to the breadcrumb.
    $currentTitle = $this->getCurrentPageTitle($route_match);

    if ($currentTitle) {
      $breadcrumb->addLink(Link::createFromRoute($currentTitle, '<none>'));
    }

    return $breadcrumb;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('request_stack')
    );
  }

  /**
   * Gets the current page's title based on the entity type and ID from the route.
   *
   * @param RouteMatchInterface $route_match
   *   The current route match.
   *
   * @throws \InvalidArgumentException
   *   Throws exception if entity type or ID is missing or invalid.
   *
   * @return string|TranslatableMarkup|null
   *   The current page title or NULL if it can't be determined.
   */
  protected function getCurrentPageTitle(RouteMatchInterface $route_match): null|string|TranslatableMarkup {
    $entity_type = $route_match->getParameter('entity_type');
    $entity_id = $route_match->getParameter('entity_id');

    // Check whether route parameters are available.
    if (!$entity_type || !$entity_id) {
      throw new \InvalidArgumentException();
    }

    // Check whether the entity type exists
    if (!$this->entityTypeManager->hasDefinition($entity_type)) {
      throw new \InvalidArgumentException();
    }

    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

    // Check whether the entity exists
    if (!$entity) {
      throw new \InvalidArgumentException();
    }

    return $entity->label();
  }

}
