<?php

namespace Drupal\entity_translate_side_by_side\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EntityTranslateSideBySideController extends ControllerBase {

  /**
   * Constructs an EntityTranslateSideBySideController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    /**
     * The entity type manager.
     */
    protected $entityTypeManager
  ) {}

  /**
   * Creates an instance of EntityTranslateSideBySideController.
   *
   * @param ContainerInterface $container
   *   The service container.
   *
   * @return EntityTranslateSideBySideController|static
   *   An instance of the EntityTranslateSideBySideController.
   */
  public static function create(ContainerInterface $container): EntityTranslateSideBySideController|static {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Title callback for the entity translation side-by-side form.
   *
   * This method is used as a route title callback. It dynamically generates
   * the page title based on the entity being edited.
   *
   * @param string $entity_type
   *   The type of the entity, e.g., 'node', 'user'.
   * @param int|string $entity_id
   *   The ID of the entity to be loaded.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \InvalidArgumentException
   *
   * @return TranslatableMarkup|string|null
   *   The label of the entity or NULL if the entity does not have a label.
   */
  public function titleCallback(string $entity_type, int|string $entity_id): null|string|TranslatableMarkup {
    // Check whether the entity type exists
    if (!$this->entityTypeManager->hasDefinition($entity_type)) {
      throw new \InvalidArgumentException();
    }

    // Check whether the entity exists
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);

    if (!$entity) {
      throw new \InvalidArgumentException();
    }

    return $this->t('Entity translation for @entity_label type of @entity_type', [
      '@entity_label' => $entity->label(),
      '@entity_type' => $entity_type,
    ]);
  }

}
