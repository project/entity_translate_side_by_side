/**
 * @file
 * Edit form.
 *
 * This file contains the behaviors and functions for the edit form, specifically
 * for updating the language selection.
 */
(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.edit_form_scripts = {

    /**
     * Attaches behaviors for the edit form.
     *
     * This function is called by Drupal to attach behaviors to the page. It binds
     * a click event to the 'Update Languages' link. When clicked, it triggers
     * the function to update the language selection based on checked checkboxes.
     *
     * @param {Object} context - The context within which the behavior is being applied.
     */
    attach: function (context) {
      $(once('update-languages-action', '#update-languages-link', context)).on('click', function (e) {
        e.preventDefault();
        Drupal.behaviors.edit_form_scripts.updateLanguageSelection(context);
      });
    },

    /**
     * Updates the language selection and reloads the page with new query parameters.
     *
     * This function collects the values of checked language checkboxes and
     * reloads the page with these values as query parameters. It is used to
     * dynamically update the form based on the user's language preferences
     * without submitting the form in the traditional way.
     *
     * @param {Object} context - The context within which the behavior is being applied.
     */
    updateLanguageSelection: function(context) {
      let selectedLanguages = [];
      $('input.form-checkbox[name^="language_selection"]', context).each(function() {
        if ($(this).is(':checked')) {
          selectedLanguages.push($(this).val());
        }
      });

      // Extract the 'destination' parameter from the current URL.
      const urlParams = new URLSearchParams(window.location.search);
      const destination = urlParams.get('destination');

      // Create the new URL, including the selected languages and the 'destination' parameter, if available.
      let newUrl = window.location.pathname + '?langcodes=' + selectedLanguages.join(',');
      if (destination) {
        newUrl += '&destination=' + encodeURIComponent(destination);
      }

      window.location.href = newUrl;
    },

  };

})(jQuery, Drupal, once);
