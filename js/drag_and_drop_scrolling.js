/**
 * @file
 * Global utilities.
 *
 * This script implements drag-and-drop scrolling functionality for the scrollable entity translate side by side container.
 * It enables users to click and drag within the container to scroll horizontally.
 * The behavior is attached to the 'languages-edit-wrapper' element and includes event listeners
 * for mouse actions to manage the dragging state and scroll position.
 */
(function ($, Drupal, once) {

  'use strict';

  Drupal.behaviors.drag_and_drop_scrolling = {

    /**
     * Attach function to initialize drag-and-drop scrolling.
     *
     * @param {Object} context - The context within which the behavior is being applied.
     */
    attach: function (context) {
      document.addEventListener('DOMContentLoaded', function() {
        const scrollContainer = document.getElementById('languages-edit-wrapper');

        let isDown = false;
        let startX;
        let scrollLeft;

        scrollContainer.addEventListener('mousedown', (e) => {
          isDown = true;
          scrollContainer.classList.add('active');
          startX = e.pageX - scrollContainer.offsetLeft;
          scrollLeft = scrollContainer.scrollLeft;
        });

        scrollContainer.addEventListener('mouseleave', () => {
          isDown = false;
          scrollContainer.classList.remove('active');
        });

        scrollContainer.addEventListener('mouseup', () => {
          isDown = false;
          scrollContainer.classList.remove('active');
        });

        scrollContainer.addEventListener('mousemove', (e) => {
          if (!isDown) return;
          e.preventDefault();
          const x = e.pageX - scrollContainer.offsetLeft;
          const walk = (x - startX) * 2;
          scrollContainer.scrollLeft = scrollLeft - walk;
        });
      });

    },

  };

})(jQuery, Drupal, once);
